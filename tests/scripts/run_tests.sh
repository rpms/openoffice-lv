#!/bin/bash

set -e

echo "=== Tests for hunspell-lv ==="
python3 test_dic_availability.py
echo ""
python3 test_suggest_words.py

echo ""
echo ""

echo "=== Tests for hyphen-lv ==="
gcc -Wall test-hyphen.c -o test-hyphen -lhyphen

echo "Test to hyphenate given word => pretpulksteņrādītājvirziens"
echo "pretpulksteņrādītājvirziens" | ./test-hyphen /usr/share/hyphen/hyph_lv_LV.dic /dev/stdin

echo ""

echo "Test to give all possible ways to hyphenate the given word => pretpulksteņrādītājvirziens"
echo "pretpulksteņrādītājvirziens" | ./test-hyphen -d /usr/share/hyphen/hyph_lv_LV.dic /dev/stdin

